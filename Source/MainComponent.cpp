/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");
    midiLabel.setText("Welcome...",dontSendNotification);
    midiLabel.setBounds(100,100,200,200);
    addAndMakeVisible(midiLabel);

}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{

}

void MainComponent::handleIncomingMidiMessage(MidiInput *source, const MidiMessage &message)
{
    DBG("MidiInput\n");
    String midiText;
    
    
    if (message.isNoteOnOrOff())
    {
        midiText << "NoteOn: Channel " << message.getChannel();
        midiText << ":Number" << message.getNoteNumber();
        midiText << ":Velocity " << message.getVelocity();
    }
    else if (message.isPitchWheel())
    {
        midiText << "PitchWheel: Channel " << message.getChannel();
        midiText << ":Value " << message.getPitchWheelValue();
    }
    else if (message.isProgramChange())
    {
        midiText << "ProgramChange: Channel " << message.getChannel();
        midiText << ":Number " << message.getProgramChangeNumber();
    }
    else if (message.isController())
    {
        midiText << "ControlChange: Channel " << message.getChannel();
        midiText << ":Number " << message.getControllerNumber();
        midiText << ":Value " << message.getControllerValue();
    }
    else if (message.isAftertouch())
    {
        midiText << "Aftertouch: Channel " << message.getChannel();
        midiText << ":Value " << message.getAfterTouchValue();
    }
    else if (message.isChannelPressure())
    {
        midiText << "ChannelPressure: Channel " << message.getChannel();
        midiText << ":Value " << message.getChannelPressureValue();
    }
    
    midiLabel.getTextValue() = midiText;
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
}