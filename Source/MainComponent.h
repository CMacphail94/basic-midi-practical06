/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MidiInputCallback
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized();
    void handleIncomingMidiMessage(MidiInput * source, const MidiMessage &message)	override;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    AudioDeviceManager audioDeviceManager;
    Label midiLabel;
    
};


#endif  // MAINCOMPONENT_H_INCLUDED
